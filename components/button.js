import styles from '../styles/button.module.css'
import {useState} from 'react'
const Button =(props)=>{
    const [count, setCount]=useState(0);
    return (<button onClick={()=> setCount(count+1)} className={styles.button}>{count}</button>)
}
export default Button;